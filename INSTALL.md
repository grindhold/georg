Installation
============

All mentioned package names are only guaranteed to be accurate
under Debian bullseye/sid.

## 1. Install Dependencies:
```
apt install qgis libopsin-java openbabel python3-fiona python3-shapely
```

## 2. Download Data Sources:

Define a folder for your data at a location of your choice
```
mkdir /location/of/your/choice/datafolder
cd /location/of/your/choice/datafolder
```

## 3. Download the required data sources:

```
wget -O geobio.zip https://www.eea.europa.eu/data-and-maps/data/biogeographical-regions-europe-3/zipped-shapefile-format-vector-polygon/zipped-shapefile-format-vector-polygon/at_download/file
wget -O marinebio.zip https://www.eea.europa.eu/data-and-maps/data/europe-seas/mpa-assessment-areas/shape-files/at_download/file
wget -O geospecies.zip https://www.eea.europa.eu/data-and-maps/data/linkages-of-species-and-habitat/linkages-of-species-and-habitat/linkages-of-species-and-habitat-1/at_download/file
wget -O duke.zip https://data.nal.usda.gov/system/files/Duke-Source-CSV.zip
```

## 4. Unpack them

```
ls -1 | xargs -I{} unzip {}
```

## 5. Convert them maps

1. Open QGIS
2. Navigate to /location/of/your/choice/datafolder in the File Browser on the lefthand side of the screen
3. Doubleclick on the file BiogeoRegions2016.shp
4. Right-click on the new entry "BiogeoRegions2016" in the Layers list below → Export → Save Feature As…
5. Choose Format: GeoJSON
6. Set filename /location/of/your/choice/datafolder/biomes\_land.geojson
7. Choose CRS: Default CRS: EPSG:4326 - WGS 84
8. Click OK
9. If you are short on RAM: Right-Click → "Remove Layer…" both layers from the Layers Browser when you are done exporting
10. Repeat from 3 onward but with the file MSFD_Marine_Subregions_draft_EU_EEZ_20130614.shp and "biomes_marine.geojson" as export filename.

## 6. Install cmljitter

```
git clone https://notabug.org/grindhold/cmljitter
cd cmljitter
sudo python3 setup.py install
cd ..
```

## 7. Clone the source


```
git clone https://codeberg.org/grindhold/georg
sed -i 's-<please set your datapath>-/location/of/your/choice/datafolder-' georg/georg/__init__.py
```

## 8. Execute georg

Ask georg for substances in the environment of a GPS coordinate in the form lon:lat
Preliminarily only european coordinates have a chance to yield results.

```
python3 georg/georg/__init__.py 13:52
```

If there have been results, the programm will write a set of enumerated sdf-files to the folder the program is executed from.

A query currently takes about 6 minutes on my machine.

Have a queryfest!
