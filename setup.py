from distutils.core import setup

setup(
    name="georg",
    version="0.2",
    description="Geocoordinate-to-Molecule resolver",
    author="Daniel 'grindhold' Brendle",
    author_email="grindhold+georg@skarphed.org",
    license="GPLv3",
    
    packages=[
        "georg"
    ]
)
